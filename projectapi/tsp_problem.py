﻿"""This module implements the Farthest Insertion algorithm logic for
approximating a good solution to a Traveling Salesman Problem.

To use the algorithm, call tsp_farthest_insertion().

>>> distance_matrix = [[0, 10, 20], [9, 0, 30], [19, 29, 0]]
>>> tsp_farthest_insertion(distance_matrix)
[0, 2, 1, 0]
"""

import random

def tsp_farthest_insertion(distance_matrix):
    """Returns a route generated with the Farthest Insertion algorithm.
    This is the main function of the module, where the logic is put together.

    The implementation of the algorithm is loosely based on:
    www.mafy.lut.fi/study/DiscreteOpt/CH5_3.pdf 
    [p. 43, last called on 14.01.2016]
    
    Args:
        distance_matrix: A list of list, where the inner elements are ints.
    
    Returns:
        A list of indices which make up a route as a solution to the TSP.

    >>> distance_matrix = [[ 0, 10, 20], [ 9,  0, 30], [19, 29,  0]]
    >>> tsp_farthest_insertion(distance_matrix)
    [0, 2, 1, 0]
    """
    start_index = 0  # Our depot
    farthest_node = find_farthest_from_origin(start_index, 
                        distance_matrix[start_index])[0]
    subtour = [start_index, farthest_node, start_index]
    
    while True:
        len_subtour = len(subtour)
        len_dist = len(distance_matrix)
        if len_subtour - 1 >= len_dist:
            break
        farthest_from_subtour = find_farthest_from_subtour(subtour, 
                                    distance_matrix)
        subtour = cheapest_insertion(subtour, farthest_from_subtour, 
                      distance_matrix)
    return subtour


def cheapest_insertion(subtour, new_node, distance_matrix):
    """Returns a new subtour after compairing all possible subtours by length.

    Args:
        subtour (list): A list of integers that represent a subtour of nodes.
        new_node (int): Index of a new node to be inserted in the subtour.
        distance_matrix (list of lists): Contains distances between all nodes.

    Returns:
        A list of node indices that form a route as a solution for the TSP.
    
    >>> distance_matrix = [[ 0, 10, 20], [ 9,  0, 30], [19, 29,  0]]
    >>> cheapest_insertion([0, 1, 0], 2, distance_matrix)
    [0, 2, 1, 0]
    """
    assert len(subtour) >= 2
    all_tours = []
    
    # Insert new node to find all new subroutes possible.
    # Subtract 1 from the length because dist_matrix contains the depot.
    for index in range(len(subtour) - 1):  
        possible_tour = subtour[0:index + 1] + [new_node] + subtour[index + 1:]
        length = length_of_tour(possible_tour, distance_matrix)
        all_tours.append((possible_tour, length))
    
    # all_tours contains all possible tours and their length as tuples.
    # Find the subtour containing the new node that has the smallest length.
    best_tour = min(all_tours, key = lambda t: t[1])
    return best_tour[0]


def length_of_tour(subtour, distance_matrix):
    """Returns the length of a given tour using a given distance matrix.

    Args:
        subtour: A list of integer indices, which are nodes in a tour.
        distance_matrix: Provides the distances between nodes.

    Returns:
        The total length of a given (sub)tour.

    >>> distance_matrix = [[ 0, 10, 20], [9, 0, 30], [19, 29, 0]]
    >>> length_of_tour([0, 2, 1, 0], distance_matrix)
    58
    """
    assert len(subtour) >= 2
    cumulative_length = 0
    
    # Get the distances from the matrix and add them up.
    for index in range(len(subtour) - 1):
        origin = subtour[index]
        destination = subtour[index + 1]
        distance = distance_matrix[origin][destination]
        cumulative_length += distance
    
    return cumulative_length


def find_farthest_from_subtour(subtour, matrix):
    """Finds the node in the matrix that is farthest from the subtour nodes. 

    Args:
        subtour: List of integers that are ordered nodes, making up a subtour.
        matrix: List of lists with distances between origins and destinations.

    Returns:
        The node from the matrix that is farthest from any node in the subtour.

    >>> distance_matrix = [[ 0, 10, 20], [ 9,  0, 30], [19, 29,  0]]
    >>> find_farthest_from_subtour([0, 1, 0], distance_matrix)
    2
    """
    farthest_list = []

    for value in subtour[:-1]:  # Last value in list is start node
        farthest = find_farthest_from_origin(value, matrix[value], subtour)
        farthest_list.append(farthest)
    
    # find_farthest_from_origin returns a tuple of the form (node_index, distance)
    farthest = max(farthest_list, key = lambda item: item[1])
    
    return farthest[0] 


def find_farthest_from_origin(origin_index, row, exlude_list=[]):
    """Finds the node and its distance farthest from another given node. 
    It returns (None, None) if no new farthest node can be found.

    Args:
        origin_index (int): Index of the current node in a row.
        row (list): A list of distances from the current node to all others.
        exclude_list (Optional [list of ints]): Nodes that aren't candidates.

    Returns:
        A tuple with the index of the farthest node and the element from
        the row for this index (which is a distance of the origin to the 
        new farthest node).

    >>> find_farthest_from_origin(0, [0, 1, 2, 5, 3])
    (3, 5)
    """
    assert origin_index < len(row)

    farthest_index = None
    for run_index, distance in enumerate(row):
        # Do not include the start node or nodes already visited in the check.
        if run_index == origin_index or run_index in exlude_list:
            continue
        if farthest_index is None:
            farthest_index = run_index
        if row[farthest_index] < distance:
            farthest_index = run_index
    
    if farthest_index is None:
        return None, None

    return farthest_index, row[farthest_index]



if __name__ == '__main__':
    import doctest
    doctest.testmod()