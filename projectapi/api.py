﻿"""This module implements the API for the Traveling Salesman Problem solver.
It establishes a connection with the GoogleMaps API, which is used for 
calculating the distances between nodes.

It uses the logic implemented in tsp_problem.py to calculate an optimized 
route using the Farthest Insertion algorithm for the addresses provided
by the caller.

Functions:
    optimize_route(list_of_points, start_point, end_point)
    gpx_file_from_point_list(list_of_points): not implemented yet

Classes:
    GoogleConnection
    Point
"""

import math
import random
from collections import namedtuple
import googlemaps
import googlemaps.distance_matrix as gd
from projectapi.haversine import haversine
from projectapi.tsp_problem import tsp_farthest_insertion



def optimize_route(list_of_points, depot):
    """Takes a list of points and returns an optimized route for these points.
    This is the main function of the module. It uses the logic from tsp_problem
    and determines a good route for a Traveling Salesman Problem.

    Args:
        list_of_points: All nodes (Point objects) that have to be served.
        depot: A Point object where the route starts and ends.
    
    Returns:
        A route including all nodes, determined with Farthest Insertion. 
        The first and last nodes in the list are the depot.
    """
    # Get all data from files and distances from Google API:
    # list_of_addresses is a list of strings: 
    # The first one is the depot, then the rest of the nodes as strings.
    list_of_addresses = ([depot.get_address_or_gps_string()] + 
            [point.get_address_or_gps_string() for point in list_of_points])
    google_conn = GoogleConnection()
    google_matrix_return = google_conn.distance_matrix(list_of_addresses)
    resolved_list_of_addresses = google_matrix_return.resolved_points
    
    matrix = google_matrix_return.distance_matrix
    
    # This is where the logic happens:
    optimized_indices = tsp_farthest_insertion(matrix)
    optimized_route = ([resolved_list_of_addresses[index] 
        for index in optimized_indices])  # list of strings
    optimized_route = [Point(None, None, str) for str in optimized_route]
    
    return optimized_route


def gpx_file_from_point_list(list_of_points):
    """Creates a gpx file from a given list of 'Point' objects.
    This method is yet to be implemented.
    Args:
        list_of_points: A list of Point objects.

    Returns:
        A string containing a created gpx file.
    """
    #TODO: Implement me
    raise NotImplementedError


# A named tuple for saving the results from the Google connection:
GoogleMatrixReturn = namedtuple('GoogleMatrixReturn', 
                     ['resolved_points', 'distance_matrix', 'length'])

class GoogleConnection(object):
    """This class implements the connection with the GoogleMaps services
    to get a distance matrix for a list of GPS points.


    For detailed documentation on the GoogleMaps API please refer to
    https://googlemaps.github.io/google-maps-services-python/docs/2.4.2/

    Attributes:
        api_key
        language
        __connect()
    """
    def __init__(self, language='de'):
        self.api_key = 'AIzaSyCXeByBGPOGQd7lffKB-kYJ9wqSYkdlm1g'
        self.language = language
        self.__connect()

        
    def __connect(self):
        self.client = googlemaps.Client(key = self.api_key)


    def distance_matrix(self, list_of_addresses):
        """Retrieves a distance matrix for given addresses using the 
        Google API.

        The distance matrix is of dimension n * n (where n is the length of 
        the list of addresses). Each row of the matrix contains one origin
        paired with each destination. The index of the matrix will be the 
        index of the entries in list_of_addresses.

        Args:
            list_of_addresses: A list of strings.
        
        Returns:
            A named tuple containing resolved points, a distance matrix
            containing the distances between all nodes and the length
            of the matrix (which is n).

        >>> address_list = ['Graz']
        >>> result = GoogleConnection().distance_matrix(address_list)
        >>> result.resolved_points
        [u'Graz, \\xd6sterreich']
        >>> result.distance_matrix
        [[0]]
        """
        request_return = gd.distance_matrix(self.client, list_of_addresses, 
                            list_of_addresses, language=self.language, 
                            mode='driving', units='metric')
        
        if request_return['status'] != 'OK':
            raise RuntimeError('Google Distance Matrix call failed')
        
        matrix = []
        # request_return is a dict, that's why we can access what we need 
        # via the keys like below:
        for row in request_return['rows']:
            matrix.append([elem['distance']['value'] for elem in row['elements']])
        
        # resolved_points: google translates user input into certain addresses
        return GoogleMatrixReturn(
            resolved_points = request_return['destination_addresses'],
            distance_matrix = matrix,
            length = len(matrix))



class Point(object):
    """This class defines a type of point needed for route calculation.
    Each point has a latitude and longitude value and can also have a
    string as an address (default for address is set to None).

    Attributes:
        latitude
        longitude
        address (Optional [str])
    """
    def __init__(self, latitude, longitude, address=None):
        self.latitude = latitude
        self.longitude = longitude
        self.address = address


    def get_euclidean_distance_to_other(self, otherPoint):
        """Returns the euclidean distance in km between self and a given 
        'otherPoint' (has to be a Point object) using the haversine-module.
        
        Just for testing - is not used actively used at the moment.
        
        Args:
            otherPoint: Point object as specified by the class.
        
        Returns: 
            The euclidean distance of two points, determined with haversine.
       
        >>> Point(3.0, 10.0).get_euclidean_distance_to_other(Point(2.0, 20.0))
        1116.425315468828
        """
        tuple1 = (self.latitude, self.longitude)
        tuple2 = (otherPoint.latitude, otherPoint.longitude)
        return haversine(tuple1, tuple2)


    def get_address_or_gps_string(self):
        """Gets attributes from a Point object."""
        if self.latitude and self.longitude:
            return '{:f}, {:f}'.format(self.latitude, self.longitude)
        return self.address
        

    def __str__(self):
        """Generates better formatted string representation for the class."""
        coordinate_string = 'lat={} lon={}'.format(self.latitude, self.longitude)

        if self.address is not None:
            return self.address + " (" + coordinate_string + ")"
        else:
            return 'Point object: ' + coordinate_string