"""Some unittests for gisdata.gisRead"""
import unittest
from gisdata.gisRead import *


class test_gisRead(unittest.TestCase):
    def setUp(self):
        self.path = path.abspath("gisdata/gis")
        self.list_files = list_files(self.path)

    def test_if_csv_exist(self):
        self.assertTrue(self.list_files[1].endswith("csv"))

    def test_list_files(self):
        self.assertNotEqual(len(self.list_files), 0)

if __name__ == '__main__':
    unittest.main()
