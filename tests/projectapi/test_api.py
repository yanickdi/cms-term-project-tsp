﻿"""Some unit tests for the package projectapi.api."""

import unittest
from projectapi.api import GoogleConnection
from projectapi.api import Point
from projectapi.api import optimize_route


class TestOptimizeRoute(unittest.TestCase):
    def setUp(self):
        self.test_1_point = Point(0, 98)
        self.test_2_points = [Point(0, 98), Point(100, 0)]

    def test_optimize_route(self):
        # TODO: implement me!
        pass


class TestGPXFile(unittest.TestCase):
    def setUp(self):
        pass


class TestGoogleConnection(unittest.TestCase):
    """Test class for projectapi.api.GoogleConnection"""
    
    def setUp(self):
        self.conn = GoogleConnection()
        self.test_route_mixed = ['Graz', 'Wien', 'Salzburg', 
            '4553 Schlierbach, Prügelmühleweg 5', 'Kirchdorf an der Krems']
        self.test_r = self.conn.distance_matrix(self.test_route_mixed)


    # Tests for GoogleConnection.distance_matix(self, list_of_addresses)
    def test_length_distance_matrix(self):
        """Tests the length of the retrieved distance matrix"""
        r = self.conn.distance_matrix(self.test_route_mixed)
        self.assertEqual(len(self.test_route_mixed), len(self.test_r.resolved_points))

    def test_distance_matrix_diagonals(self):
        """Tests if all diagonals in the matrix are zero."""
        for i in range(0, self.test_r.length):
            self.assertEqual(self.test_r.distance_matrix[i][i], 0)


class TestPoint(unittest.TestCase):
    """Testclass for projectapi.api.Point"""
    
    def setUp(self):
        self.test_p1 = Point(4.0, 2.0)
        self.test_p2 = Point(3.0, 1.0)

    def test_euclidean_distance(self):
        self.assertEqual(self.test_p1.get_euclidean_distance_to_other(self.test_p2),
            157.10577709637062)

    def test_euclidean_distance_same_point(self):
        self.assertEqual(self.test_p1.get_euclidean_distance_to_other(self.test_p1),
            0.0)


if __name__ == '__main__':
    unittest.main()