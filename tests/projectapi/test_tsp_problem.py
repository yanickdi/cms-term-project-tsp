"""Unit tests for projectapi.api.tsp_problem"""

import unittest
from projectapi.tsp_problem import *


class TestTSPproblem(unittest.TestCase):
    """Testclass for the TSP solver logic"""
    def setUp(self):
        self.test_matrix_1_node = [[0]]
        self.test_matrix_2_nodes = [[0,  98],
                                    [100, 0]]
        self.test_simple_matrix = [[ 0, 10, 20],
                                   [ 9,  0, 30],
                                   [19, 29,  0]]
        self.test_equal_dist_matrix = [[0, 9, 9, 9], 
                                       [9, 0, 9, 9],
                                       [9, 9, 0, 9], 
                                       [9, 9, 9, 0]]
        self.test_matrix_4_nodes = [[0,       476713, 295999, 763840],
                                    [476288,  0,      185871, 914578],
                                    [295190,  186955, 0,      754108],
                                    [764507,  916333, 754565, 0]]
        self.test_matrix_5_nodes = [[0,       2381,   2242,   620,    15090],
                                    [2257,    0,      1678,   2372,   16842],
                                    [2604,    1561,   0,      2719,   19943],
                                    [532,     2793,   2803,   0,      14751],
                                    [15004,   17265,  19956,  14753,  0]]

    
    # Tests for tsp_farthest_insertion(distance_matrix) 

    def test_tsp_solution_single_value_matrix(self):
        self.assertEqual(tsp_farthest_insertion(self.test_matrix_1_node),
                            [0, None, 0])

    def test_tsp_solution_correctness_small_input(self):    
        self.assertEqual(tsp_farthest_insertion(self.test_matrix_2_nodes),
                            [0, 1, 0])

    def test_tsp_solution_correctness_simple_matrix(self):
        self.assertEqual(tsp_farthest_insertion(self.test_simple_matrix),
                            [0, 2, 1, 0])

    def test_tsp_solution_correctness_large_input(self):
        self.assertEqual(tsp_farthest_insertion(self.test_matrix_5_nodes),
                            [0, 2, 1, 3, 4, 0])

    def test_tsp_solution_equal_distances_matrix(self):
        self.assertEqual(tsp_farthest_insertion(self.test_equal_dist_matrix),
                            [0, 3, 2, 1, 0])
    

    # Tests for cheapest_insertion(subtour, new_node, distance_matrix)
    def test_cheapest_insertion_invalid_new_node(self):
        self.assertRaises(IndexError, cheapest_insertion, [0, 1, 0], 20,
                            self.test_simple_matrix)

    def test_cheapest_insertion_invalid_subtour(self):
        self.assertRaises(AssertionError, cheapest_insertion, [0], 1,
                            self.test_simple_matrix)

    def test_cheapest_insertion_unambiguous_valid_input(self):
        self.assertEqual(cheapest_insertion([0, 1, 0], 2, 
                            self.test_simple_matrix), [0, 2, 1, 0])

    def test_cheapest_insertion_ambiguous_valid_input(self):
        self.assertEqual(cheapest_insertion([0, 2, 1, 0], 3, 
                            self.test_equal_dist_matrix), [0, 3, 2, 1, 0])


    # Tests for length_of_tour(subtour, distance_matrix)
    def test_length_of_tour_invalid_subtour(self):
        self.assertRaises(AssertionError, length_of_tour, 
                              [0], self.test_matrix_1_node)

    def test_length_of_tour_2_nodes_subtour(self):
        self.assertEqual(length_of_tour([0, 1, 0], self.test_matrix_2_nodes),
                            198)

    def test_length_of_tour_3_nodes_simple_matrix(self):
        self.assertEqual(length_of_tour([0, 2, 1, 0], self.test_simple_matrix),
                            58)

    def test_length_of_tour_3_nodes_subtour_large_dist(self):
        self.assertEqual(length_of_tour([0, 2, 3, 0], self.test_matrix_4_nodes),
                            1814614)


    # Tests for find_farthest_from_subtour(subtour, matrix)
    def test_farthest_from_subtour_invalid_origin(self):
        self.assertRaises(IndexError, find_farthest_from_subtour, 
                            [0, 15, 0], self.test_simple_matrix)

    def test_farthest_from_subtour_unambiguous_simple_input(self):
        self.assertEqual(find_farthest_from_subtour([0, 2, 0], 
                            self.test_simple_matrix), 1)

    def test_farthest_from_subtour_equal_distances_matrix(self):
        self.assertEqual(find_farthest_from_subtour([0, 1, 0], 
                            self.test_equal_dist_matrix), 2)
    

    # Tests for find_farthest_from_origin(origin_index, row, exlude_list = [])
    def test_farthest_from_origin_invalid_origin(self):
        self.assertRaises(AssertionError, find_farthest_from_origin, 3, 
                            [0, 15])

    def test_farthest_from_origin_all_nodes_included(self):
        self.assertEqual(find_farthest_from_origin(1, [9, 0, 9, 9], 
                            [0, 3, 2, 1]), (None, None))

    def test_farthest_from_origin_equal_distances(self):
        self.assertEqual(find_farthest_from_origin(0, [0, 9, 9, 9]),
                            (1, 9))



if __name__ == '__main__':
    unittest.main()