"""Some unit tests for module projectapi.api.haversine"""

import unittest
from projectapi.haversine import haversine


class TestHaversine(unittest.TestCase):
    def setUp(self):
        self.test_point_1 = (45.7597, 4.8422)
        self.test_point_2 = (48.8567, 2.3508)
    
    def test_input_same_point(self):
        self.assertAlmostEqual(haversine(self.test_point_1, self.test_point_1), 
                               0.00)

    def test_input_p1p2(self):
        self.assertAlmostEqual(haversine(self.test_point_1, self.test_point_2),
                               392.21671780659625)

    def test_p1p2_output_miles(self):
        self.assertAlmostEqual(haversine(self.test_point_1, self.test_point_2,
                                         True),
                               243.71209416020253)