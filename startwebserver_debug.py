import sys
import webserver.application

if __name__ == '__main__':
    app = webserver.application.app
    app.debug = True
    app.run(port=8000)