#!/usr/bin/env python3
from wsgiref.handlers import CGIHandler
from webserver.application import app

if __name__ == '__main__':
    CGIHandler().run(app)
