﻿"""
This module offers a WSGI Flask Application that offers a JSON API Interface
to the projectapi and renders also some html for the Web UI.
"""

import json
import os.path
from flask import Flask, render_template, jsonify, request, url_for
from projectapi import api

app = Flask(__name__)

@app.route('/')
def index():
    """A view for the URL: /"""
    return render_template('map.html')

@app.route('/sphinx')
def sphinx():
    """A view for the URL: /sphinx"""
    sphinx_index = os.path.join(app.static_folder, 'sphinx/index.html')
    sphinx_built = os.path.isfile(sphinx_index)
    return render_template('sphinx.html', sphinx_built = sphinx_built)


@app.route('/api/solve', methods=['POST'])
def api_solve():
    """
    A JSON Interface to the project api for browser ajax request at /api/solve
    
    The Request must use the HTTP POST method. It must contain only one POST
    argument called `addresses`. This argument must be a JSON encoded dictionary, like:
    { request_addresses : ['Vienna', 'Graz', 'Paris'] }
    The first item in the `request_addresses` has to be the TSP depot. The others are waypoints on the route.
    
    Responses:
    A JSON Dictionary like:
    {
      request_addresses : list_of_requested_addresses,
      optimized_route : list_of_optimized_address_list
    }

    The `optimized_route` is the output of the TSP algorithm with input `request_addresses`
    """
     
    request_dict = json.loads(request.form['json'])
    address_list = request_dict['addresses']
    
    depot = api.Point(None, None, address_list[0])
    list_of_points = [api.Point(None, None, address) for address in address_list[1:] ]
    
    optimized_route = api.optimize_route(list_of_points, depot)
    
    optimized_route = [point.address for point in optimized_route]
    json_dict = {
        'request_addresses' : request_dict['addresses'],
        'optimized_route' : optimized_route
    }
    return jsonify(json_dict)

