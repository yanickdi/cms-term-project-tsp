﻿$(function () {
    $('#solve').click(do_solve);

    //$('#addresses').val('Wien\nInnsbruck\nSalzburg\nSplit, Croatia');
});

function do_solve(ev) {
    ev.preventDefault();
    var addresses = $('#addresses').val().split('\n');
    if (addresses.length < 2){
      alert('Minimum is two waypoints!');
      return;
    }
    var json_data = {
        'addresses' : addresses
    };
    $('#solve').toggleClass('hide');
    $('#loading').toggleClass('hide');
    $.ajax({
        type: 'POST',
        url: $('#solve').data('ajaxUrl'),
        data: { json: JSON.stringify(json_data) },
        success: solve_success,
        error: function (err) { console.log(err); alert(err.statusText + ', see also browser log') }
    });
}

function solve_success(data) {
    $('#loading').toggleClass('hide');
    $('#result_box').toggleClass('hide');

    var optimized_route = data['optimized_route'];
    var textboxstring = optimized_route.join('\n');
    $('#addresses').val(textboxstring);

    displayRoute(optimized_route);
}

function displayRoute(route) {
    var gmapobj = $('#map').data('gmapobj');
    var service = new google.maps.DirectionsService;
    var display = new google.maps.DirectionsRenderer({
        draggable: false,
        map: gmapobj
    });
    
    var list_of_waypoints = route.slice(1, -1);
    var waypoints = [];
    for (var i = 0; i < list_of_waypoints.length; i++)
        waypoints.push({ location: list_of_waypoints[i] });

    service.route({
        origin: route[0],
        destination: route[route.length-1],
        waypoints: waypoints,
        travelMode: google.maps.TravelMode.DRIVING,
        avoidTolls: false
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            display.setDirections(response);
        } else {
            alert('Could not display directions due to: ' + status);
        }
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 47.07077, lng: 15.4395 },
        zoom: 6
    });
    $('#map').data('gmapobj', map);
}