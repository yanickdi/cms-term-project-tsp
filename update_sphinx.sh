#!/usr/bin/env bash
cd sphinx
make html
cp -r _build/html/* ../webserver/static/sphinx/
echo 'sphinx updated to /webserver/static/sphinx/'
