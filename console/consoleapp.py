﻿"""This module implements the command line interface for the TSP solver.
It imports the functionality from the project API (projectapi/api.py).

To use the interface, execute 'startconsole.py' from the main directory:
cms-term-project-tsp $ python3 startconsole.py
You can test the function using the following files:
tests/track1.txt
tests/track2.txt
"""

import os.path
import re
from projectapi.api import Point, optimize_route


ERR_MESSAGE_FILE_NOT_FOUND = """Could not find the file specified.
Please check file name and path."""
USER_INPUT_PROMPT = "Enter a file with addresses to get an optimized route: "

def main():
    """Takes a file with addresses, builds a route and prints it to stdout."""

    # Check if file exists.
    filename = os.path.abspath(input(USER_INPUT_PROMPT))
    if os.path.isfile(filename) == False:
        print(ERR_MESSAGE_FILE_NOT_FOUND)
        return 2

    # Read file data to get a list of point objects.
    list_of_points = get_point_list_from_text_file(filename)

    # Optimize a route using the project API.
    # First argument to optimize_route are all nodes, second is the depot.
    optimized_route = optimize_route(list_of_points[1:], list_of_points[0])

    # Print the optimized route to stdout.
    print('\nYour optimized route determined by the Farthest Insertion algorithm:\n')
    for point in optimized_route:
        print(point)

    return 0


# Check if there are coordinates given in the file using regular expressions.
_REGULAR_EXPRESSION_GPS = re.compile(r'^(\d{2}\.\d+), ?(\d{2}\.\d+)$')

def get_point_list_from_text_file(filename):
    """Reads a text file line by line, reads the address string out of it, or, 
    if it is in the right format, reads a GPS point (latitude, longitude).
    
    Args:
        filename (str): A user defined file with input data (addresses).
    Returns:
        A list of 'Point' objects, which have coordinates if specified in the
        file (and None as an address string), or None for no coordinates and 
        an address string.

    >>> result = get_point_list_from_text_file('tests/track1.txt')
    >>> result[0].get_address_or_gps_string()
    '47.069538, 15.435496'
    """
    point_list = []

    with open(filename, mode='r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            match = re.search(_REGULAR_EXPRESSION_GPS, line)
            if match is None:  # It's an address
                new_point = Point(None, None, line)
            else:
                new_point = Point(float(match.group(1)), float(match.group(2)))
            point_list.append(new_point)
    
    return point_list


if __name__ == '__main__':
    import doctest
    doctest.testmod()