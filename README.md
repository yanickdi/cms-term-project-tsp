# CMS Term Project #

The project now runs on my RaspberryPi2! Take a look at: http://pi.dickbauer.at/cgi-bin/cms.cgi/ (nope, mod_rewrite is not installed yet).

To get the term project running you have to follow these steps:

1. Install [git](https://git-scm.com/downloads) on your pc/mac/linux
2. Open a terminal and change the dir to the folder you want to install the project
3. type in:

```
#!shell

git clone https://your_username@bitbucket.org/yanickdi/cms-term-project-tsp.git
cd cms-term-project-tsp
pip3 install -r requirements.txt
```

There are two user interfaces:

* python3 startconsole.py

or

* python3 startwebserver_debug.py  (starts a webserver on http://localhost:8000)

## Unittesting ###
run
```
#!shell
python start_unittests.py
```