#!/usr/bin/env python3

"""
This module lists files and
reads content from CSV files in given folder (default 'gis')
"""

from os import path
from os import walk
# from collections import namedtuple
3
GIS_FOLDER = 'gis'  # Update this if needed
gis = path.abspath(GIS_FOLDER)


def list_files(path):
    """This function lists all files in a given path.
    Args:
        path (str): An absolute path name.
    Returns:
        file_list: A list of all files in the given path.

    >>> list_files(gis)
    ['8430LeibnitzTillmitsch.csv', '8411Hengsberg.csv', '8431Gralla.csv',
     '8081heiligenkreuz.csv', '8410Wildon.csv',
     '8082ZerlachKirchbachFranach.csv', '8413RagnitzStGeorgen.csv',
     '8403LebringLang.csv', 'gis8072Mellach.csv', '8412Allerheiligen.csv']
    """
    files = []
    for elem in walk(path):
        files.append(elem)
    file_list = files[0][2]
    return file_list


def csvlist(path, ext='.csv'):
    """This function lists all csv files in a given path.
    Args:
        path (str): An absolute path name.
        ext (str):  A file extension (defaults to .csv)
    Returns:
        csv_files:  A list of csv files in a given path.
    >>> csvlist(gis)
    ['8082ZerlachKirchbachFranach.csv', '8403LebringLang.csv', '8412Allerheiligen.csv', '8410Wildon.csv', '8411Hengsberg.csv', '8413RagnitzStGeorgen.csv', '8431Gralla.csv', 'gis8072Mellach.csv', '8430LeibnitzTillmitsch.csv']
    >>> 
    """
    csv_files = []
    for elem in list_files(path):
        if elem.endswith(ext):
            csv_files.append(elem)
    return csv_files


def readCsvFiles(csvlist):
    """
    This function reads CSV files from a list.
    Args:
        csvlist (list): A list of CSV files.
    Returns:
        p (list):
    >>> readCsvFiles(csvlist(gis))[1]
    '8082,Breitenbuch 60,15.66178169,46.97162528,Breitenbuch'
    >>> 
    """
    p = []
    for elem in csvlist:
        f = gis + "/" + elem
        with open(f, mode='r', encoding='utf-8', errors=None,
                  newline='\n', closefd=True, opener=None) as fl:
            d = [j.rstrip() for j in fl]
            p = p + d
    return p


if __name__ == '__main__':
        p = readCsvFiles(csvlist(gis))
        v = []
        for i in p:
            l = i.split(",")
            v.append(l)

        for i in v:
            print(i[0], i[1], i[2], i[3], i[4])
