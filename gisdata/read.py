#!/usr/bin/env python3

"""
This module stores coordinates from a csv files in a list of namedtuple,
and searches for particular address with a given pattern. Results are
stored eather in adressList.txt file or in a list of namedtuples.
"""

from collections import namedtuple
import csv
import re
import gisRead as gr


csv_list = gr.GIS_FOLDER
database = gr.csvlist(gr.gis)


def base(path):
    """
    Makes a list of namedtuple from csv files.
    Args:
        path (str): An apsolite path name.
    Returns:
        base: a list of:
              namedtuple('Address', 'Order, Front, lon, lat, Note')
    >>> base(csv_list + "/" + "8410Wildon.csv")[1]
    Address(Order='8410', Front='Ackerweg 1', lon='15.48005113', lat='46.89302655', Note='Weitendorf')
    >>> 
    """
    base = []
    addresses = namedtuple('Address', 'Order, Front, lon, lat, Note')
    for address in map(addresses._make, csv.reader(
            open(path, "r", encoding='utf-8'))):
        #    print(address.Front, address.lat, address.lon, address.Order)
        base.append(address)
    return base


def base_from_csvlist(csv_list):
    """
    Returns a list of namedtuple from a list of csv files.
    Args:
        csv_list
    Returns:
        dd: a list of namedtuple from all files in given csv_files list.
    >>> base_from_csvlist(csv_list)[1][1]
    Address(Order='8403', Front='Alte Reichsstraße 1', lon='15.53829868', lat='46.84254581', Note='Lebring')
    >>> 
    """
    dd = []
    for elem in database:
        dd.append(base(csv_list + "/" + elem))
    return dd


def findaddress(pattern, result=[]):
    """
    Returns a list of namedtuple(address, order, front, lon, lat, note)
    for given pattern.
    Args:
        pattern: pattern/text to find in address name field
        result: in default stores to empty list
    Returns:
        result: a list of namedtuples of addresses for particular  pattern
    >>> findaddress("afram")[1]
    Address(Order='8410', Front='Afram 10', lon='15.53734722', lat='46.89493427', Note='Afram')
    >>> 
    """
    data = base_from_csvlist(csv_list)
    for i in data:
        for j in i:
            ac = re.findall(re.compile(pattern, re.IGNORECASE), j.Front)
            if ac != []:
                result.append(j)
    return result


def write_to_file(addresspattern, filename="addressList.txt"):
    """
    Writes Coordinates of Addresses to a given file
    for a searched pattern.
    Args:
        pattern: string to search in addresses
        filename: defaults to "addressList.txt"
    Returns: 
        0
    >>> write_to_file("afram")
    0
    >>> 
    """
    pattern = addresspattern
    opened = open(filename, 'w+', encoding='utf-8')
    adr = findaddress(pattern)
    a = len(adr)
    while a > 1:
        for k in adr:
            # print(k.Front + ":", k.lat, k.lon)
            # opened.writelines(k.Front)
            # opened.writelines(": ")
            opened.writelines(k.lat)
            opened.writelines(", ")
            opened.writelines(k.lon)
            opened.writelines("\n")
            a -= 1
    opened.close()
    return 0


def empty_file(filename="addressList.txt"):
    """
    Makes a file with a content an empty file.
    Deletes contents of a file.
    >>> empty_file("addressList.txt")
    0
    >>> 
    """
    opened = open(filename, 'w', encoding='utf-8').close()
    return 0


if __name__ == '__main__':
    pattern = "aframberg 8"
    # print(findaddress(pattern)) # Somehow this line
    write_to_file(pattern)               # and this line can not be uncommented at same time, otherwise it prints and writes double :(
    # empty_file()                # for testing use comment/unceoment for these options: 
    # (print(findaddress(pattern), write_to_file(), empty_file()
