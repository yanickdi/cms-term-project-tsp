#!/usr/bin/env python3

"""
Trying to make a search app using PyQt4
"""

import sys
from PyQt4 import QtGui
from PyQt4 import QtCore

"""
class adress_table(QtCore.QTableWidget):
    def __init__(self, data, *args):
        QtCore.QTableWidget.__init__(self, *args)
        self.data = data
"""
class gisgui(QtGui.QWidget):
    def __init__(self):
        super(gisgui, self).__init__()
        self.initGUI()

    def initGUI(self):
        adress = QtGui.QLabel('Search')
        buttonAdd = QtGui.QPushButton("Add to List")
        addresslistlabel = QtGui.QLabel('List')
        buttonSubmit = QtGui.QPushButton("Submit")

        adressinput = QtGui.QLineEdit()
        list_view = QtGui.QTextEdit()

        grid = QtGui.QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(adress, 1, 0)
        grid.addWidget(adressinput, 1, 1)
        grid.addWidget(buttonAdd, 1, 2)

        grid.addWidget(addresslistlabel, 2, 0)
        grid.addWidget(list_view, 2, 1, 5, 1)
        grid.addWidget(buttonSubmit, 6, 2)

        self.setLayout(grid)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Adress List Maker')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = gisgui()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

