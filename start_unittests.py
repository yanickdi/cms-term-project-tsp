#!/usr/bin/env python3

import unittest
import tests.projectapi.test_api
import tests.projectapi.test_haversine
import tests.projectapi.test_tsp_problem
import tests.gisdata.test_gisRead

# Unfinished testing modules:
#import tests.gisdata.test_read
import tests.console.test_consoleapp


if __name__ == '__main__':
    testmodules = [
                   'tests.projectapi.test_api', 
                   'tests.projectapi.test_haversine',
                   'tests.projectapi.test_tsp_problem',
                   'tests.gisdata.test_gisRead',
                   'tests.console.test_consoleapp'
                   ]
                   
    suite = unittest.TestSuite() 
    for test in testmodules:
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(test))
    unittest.TextTestRunner().run(suite)