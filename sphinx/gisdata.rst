Package: gisdata
================================================
This package offers some functions that searches through .csv files and extracts the gps coordinates out of it.
It's developing is in proces.

It also offers a Qt Gui in the module gisdata.gui

=================
gisdata.gisRead
=================
   
.. automodule:: gisdata.gisRead
   :members:

=================
gisdata.read
=================
   
.. automodule:: gisdata.read
   :members:

=================
gisdata.gui
=================
   
.. automodule:: gisdata.gui
   :members:
