Package: webserver
================================================
This package offers a full Web User Interface including an interactive Google Maps map
that communicates via ajax json requests from the browser to a flask webserver. The web user interface
is only a view to the business logic in package projectapi.

To run the webserver in debug mode on localhost port 8000, there is a script called ''startwebserver_debug.py'' in the project's root directory.

To start the webserver in production mode, you can use the script ''startwebserver.cgi'' in project's root. However - if you *really* want to start the
project in production mode, you have to setup a fast-cgi communication with apache, ngnix, lighttpd, whatever.
Read also: http://flask.pocoo.org/docs/0.10/deploying/fastcgi/

=================
webserver.application
=================

.. automodule:: webserver.application
   :members:
