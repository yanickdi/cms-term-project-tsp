Package: projectapi
================================================

=================
projectapi.api
=================
   
.. automodule:: projectapi.api
   :members:
   
=================
projectapi.tsp_problem
=================
   
.. automodule:: projectapi.tsp_problem
   :members:
   

=================
projectapi.haversine
=================
   
.. automodule:: projectapi.haversine
   :members: