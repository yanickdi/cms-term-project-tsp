Package: tests
================================================

=================
tests.projectapi.test_api
=================
   
.. automodule:: tests.projectapi.test_api
   :members:
   
=================
tests.projectapi.test_tsp_problem
=================
   
.. automodule:: tests.projectapi.test_tsp_problem
   :members:
   

=================
tests.projectapi.test_haversine
=================
   
.. automodule:: tests.projectapi.test_haversine
   :members:
   
   
=================
tests.gisdata.test_gisRead
=================
   
.. automodule:: tests.gisdata.test_gisRead
   :members:
   
=================
tests.gisdata.test_read
=================
   
.. automodule:: tests.gisdata.test_read
   :members:
   